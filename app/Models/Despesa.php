<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;

class Despesa extends Model
{
    private $totalPaginas = 5;
    protected $fillable = [
            'user_id',
            'status',
            'tipo_despesa',
            'valor',
            'data_despesa',
            'form_pagamento',
    	    'descricao'
    ];

    //metodo para listar todos os dados de despesas na view index
    public function listar()
    {
        DB::beginTransaction();
        $despesas = Despesa::where('status','ativo')->paginate($this->totalPaginas);
        return $despesas;   
    }

    //metodo para salvar uma despesa pegando esses dados da view
    public function salvar($despesa)
    {
        DB::beginTransaction();
        //dd($despesa);
        $despesa = Despesa::create([
            'user_id' => auth()->user()->id,
            'status' => 'ativo',
            'tipo_despesa' => $despesa['tipo_despesa'],
            'valor'=> $despesa['valor'],  
            'data_despesa'=> $despesa['data_despesa'], 
            'form_pagamento'=>$despesa['form_pagamento'],
            'descricao' => $despesa['descricao']
        ]);

        //verifica se o cadastro foi efetuado com sucesso
        if ($despesa) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Ação cadastrada com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao cadastrar Ação'
            ];
        }
        
    }

    //metodo para excluir uma despesa pelo id
    public function deleteID($id)
    {   
        DB::beginTransaction();

        $despesa = Despesa::findOrFail($id);
        $despesa->status = 'inativo';
        $despesa->update();
        //dd($despesa);

        //verifica se houve a "exclusão" com sucesso
        if ($despesa) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Despesa excluida com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao excluir Despesa'
            ];
        }
    }

    //metodo para listar todas as despesas ativas no cadastro de processo
    public function despesas()
    {
        DB::beginTransaction();
        $despesas = Despesa::where('status','ativo')->get();
        return $despesas;
    }

    public function user()
    {
        //referncia a relaçao com Despesa
        return $this->belongsTo(User::class);
    }

    
}
