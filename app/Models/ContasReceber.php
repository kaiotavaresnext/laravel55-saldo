<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\Models\FluxoCaixa;


class ContasReceber extends Model
{
    private $totalPage = 5;
    protected $table = 'contas_receberas';
    protected $fillable = [
            'user_id',
            'status',
            'nome_cliente',
            'situacao',
            'tipo_despesa',
            'valor',
            'juros',
            'multa',
            'desconto',
            'vencimento',
            'form_pagamento',
    	    'descricao'
    ];

    //metodo para listar todos os dados de Contas a Receber na view index
    public function listar()
    {
        DB::beginTransaction();
        $contasReceberas = ContasReceber::where('status','ativo')->paginate($this->totalPage);
        return $contasReceberas;   
    }

    //metodo para salvar uma Conta a Receber pegando esses dados da view
    public function salvar($contasReceber)
    {
        DB::beginTransaction();
        //dd($contasReceber);
        $contasReceber = ContasReceber::create([
            'user_id' => auth()->user()->id,
            'status' => 'ativo',
            'nome_cliente'=> $contasReceber['nome_cliente'],
            'situacao' => $contasReceber['situacao'], 
            'tipo_despesa' => $contasReceber['tipo_despesa'],
            'valor'=> $contasReceber['valor'], 
            'juros'=> $contasReceber['juros'], 
            'multa'=>$contasReceber['multa'],
            'desconto'=>$contasReceber['desconto'],
            'vencimento'=> $contasReceber['vencimento'], 
            'form_pagamento'=>$contasReceber['form_pagamento'],
            'descricao' => $contasReceber['descricao']
        ]);

        //verifica se o cadastro foi efetuado com sucesso
        if ($contasReceber) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Conta a receber salva com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao salvar conta a receber'
            ];
        }
        
    }

    //metodo para excluir uma Conta a Receber pelo id
    public function deleteID($id)
    {   
        DB::beginTransaction();

        $contasReceber = ContasReceber::findOrFail($id);
        $contasReceber->status = 'inativo';
        $contasReceber->update();
        //dd($contasReceber);

        //verifica se houve a "exclusão" com sucesso
        if ($contasReceber) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Conta excluida com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao excluir Conta'
            ];
        }
    }

    //metodo para listar todas as contas a receber ativas no cadastro de processo
    public function contasReceberas()
    {
        DB::beginTransaction();
        $contasReceberas = ContasReceber::where('status','ativo')->get();
        return $contasReceberas;
    }

    public function user()
    {
        //referncia a relacao com contas a receber
        return $this->belongsTo(User::class);
    }

    public function fluxoCaixa()
    {
        //referncia a relacao com contas a receber
        return $this->belongsTo(FluxoCaixa::class);
    }

    public function situacao($situacao = null)
    {
        $situacoes =[
            'recebido' => 'Recebido', 
            'pendente' => 'Pendente', 
            'a vencer' => 'A vencer', 
            'vencido' => 'Vencido',
        ];
        
        if (!$situacao)
            return $situacoes;

        if ($this->user_id_transaction !=null && $situacao == 'recebido')
            return 'Recebido';

        return $situacoes[$situacao];
    }

    public function pesquisa(Array $data, $totalPage)
    {
        return $this->where(function ($query) use  ($data) {
                if (isset($data['nome_cliente']))
                    $query->where('nome_cliente','LIKE', $data['nome_cliente']);

                if (isset($data['vencimento']))
                    $query->where('vencimento', $data['vencimento']);
                
                if (isset($data['situacao']))
                    $query->where('situacao', $data['situacao']);
                    
        })//->toSql(); dd($pesquisas);
        ->paginate($totalPage);
    }
}
