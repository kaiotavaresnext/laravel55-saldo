<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\Models\FluxoCaixa;

class ContasPagar extends Model
{
    private $totalPage = 4;
    protected $table = 'contas_pagaras';

    protected $fillable = [
            'user_id',
            'status',
            'advogado',
            'situacao',
            'tipo_despesa',
            'valor',
            'juros',
            'multa',
            'desconto',
            'vencimento',
            'form_pagamento',
    	    'descricao'
    ];

    //metodo para listar todos os dados de tipo de processo na view index
    public function listar()
    {
        DB::beginTransaction();
        $contasPagaras = ContasPagar::where('status','ativo')->paginate($this->totalPage);
        return $contasPagaras;   
    }

    //metodo para salvar um tipo de processo pegando esses dados da view
    public function salvar($contasPagar)
    {
        DB::beginTransaction();
        //dd($contasPagar);
        $contasPagar = ContasPagar::create([
            'user_id' => auth()->user()->id,
            'status' => 'ativo',
            'advogado'=> $contasPagar['advogado'],
            'situacao' => $contasPagar['situacao'], 
            'tipo_despesa' => $contasPagar['tipo_despesa'],
            'valor'=> $contasPagar['valor'], 
            'juros'=> $contasPagar['juros'], 
            'multa'=>$contasPagar['multa'],
            'desconto'=>$contasPagar['desconto'],
            'vencimento'=> $contasPagar['vencimento'], 
            'form_pagamento'=>$contasPagar['form_pagamento'],
            'descricao' => $contasPagar['descricao']
        ]);

        //verifica se o cadastro foi efetuado com sucesso
        if ($contasPagar) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Conta salva com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao salvar conta'
            ];
        }
        
    }

    //metodo para excluir uma conta pelo id
    public function deleteID($id)
    {   
        DB::beginTransaction();

        $contasPagar = ContasPagar::findOrFail($id);
        $contasPagar->status = 'inativo';
        $contasPagar->update();
        //dd($contasPagar);

        //verifica se houve a "exclusão" com sucesso
        if ($contasPagar) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Conta excluida com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao excluir conta'
            ];
        }
    }

    //metodo para listar todas as acoes ativas no cadastro de processo
    public function contasPagaras()
    {
        DB::beginTransaction();
        $contasPagaras = ContaPagar::where('status','ativo')->get();
        return $contasPagaras;
    }

    public function user()
    {
        //referncia a relacao com Acao
        return $this->belongsTo(User::class);
    }
    
    public function fluxoCaixa()
    {
        //referncia a relacao com contas a receber
        return $this->belongsTo(FluxoCaixa::class);
    }

    public function situacao($situacao = null)
    {
        $situacoes =[
            'pago' => 'Pago', 
            'pendente' => 'Pendente', 
            'a vencer' => 'A vencer', 
            'vencido' => 'Vencido',
        ];
        
        if (!$situacao)
            return $situacoes;

        if ($this->user_id_transaction !=null && $situacao == 'pago')
            return 'Pago';

        return $situacoes[$situacao];
    }

    public function pesquisa($keyPesquisa, $totalPage = 4)
    {
        return $this
                    ->where('advogado' , 'LIKE' , "%{$keyPesquisa}%")
                    
                    ->paginate($totalPage);
    }
}
