<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use App\Models\ContasPagar;
use App\Models\ContasReceber;
use App\Models\Despesa;

class FluxoCaixa extends Model
{   
    private $totalPaginas = 5;
    protected $fillable = [
         
            'status',
            'saldo',
            
    ];

    //metodo para listar todos os dados de Contas a Receber na view index
    /*public function listar()
    {
        DB::beginTransaction();
        $contasReceberas = ContasReceber::where('status','ativo')->paginate($this->totalPaginas);
        return $contasReceberas;   
    }

    //metodo para salvar uma Conta a Receber pegando esses dados da view
    public function salvar($contasReceber)
    {
        DB::beginTransaction();
        //dd($contasReceber);
        $contasReceber = ContasReceber::create([
            'user_id' => auth()->user()->id,
            'status' => 'ativo',
            'nome_cliente'=> $contasReceber['nome_cliente'],
            'situacao' => $contasReceber['situacao'], 
            'tipo_despesa' => $contasReceber['tipo_despesa'],
            'valor'=> $contasReceber['valor'], 
            'juros'=> $contasReceber['juros'], 
            'multa'=>$contasReceber['multa'],
            'desconto'=>$contasReceber['desconto'],
            'vencimento'=> $contasReceber['vencimento'], 
            'form_pagamento'=>$contasReceber['form_pagamento'],
            'descricao' => $contasReceber['descricao']
        ]);

        //verifica se o cadastro foi efetuado com sucesso
        if ($contasReceber) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Conta a receber salva com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao salvar conta a receber'
            ];
        }
        
    }

    //metodo para excluir uma Conta a Receber pelo id
    public function deleteID($id)
    {   
        DB::beginTransaction();

        $contasReceber = ContasReceber::findOrFail($id);
        $contasReceber->status = 'inativo';
        $contasReceber->update();
        //dd($contasReceber);

        //verifica se houve a "exclusão" com sucesso
        if ($contasReceber) {
            DB::commit();

            return [
                'success' => true,
                'message' => 'Conta excluida com sucesso!'
            ];
        } else {
            DB::rollback();

            return [
                'sucess' => false,
                'message' => 'Falha ao excluir Conta'
            ];
        }
    }*/

    //metodo para listar todas as contas a receber ativas no cadastro de processo
    public function fluxoContas()
    {
        DB::beginTransaction();
        $fluxoContas = FluxoConta::where('status','ativo')->get();
        return $fluxoContas;
    }

    public function user()
    {
        //referncia a relfluxoCaixa com fluxoCaixa
        return $this->belongsTo(User::class);
    }

   //referencia a relacao com acao
   public function contasPagar()
   {
       return $this->belongsTo(ContasPagar::class);
   }

    public function contasReceber() 
    {
        //hasOne para fazer o relacionamento um para um
        return $this->belongsTo(ContasReceber::class);
    }

    public function despesa() 
    {
        //hasOne para fazer o relacionamento um para um
        return $this->belongsTo(Despesa::class);
    }

    
}
