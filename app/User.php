<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\ContasReceber;
use App\Models\ContasPagar;
use App\Models\Despesa;
use App\Models\FluxoCaixa;
use DB;



class User extends Authenticatable
{
        private $totalPaginas = 5; 
    
        use Notifiable;
    
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name', 'email', 'password', 'image'
        ];
    
        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];
    
        //listar todos os usuarios para o administrador
        public function listar()
        {
            
            //dd($users = User::all());
    
            $users = User::where('name','!=','Next Support')->paginate($this->totalPaginas);
            return $users;
        }
    
        //metodo para salvar um novo usuário
        public function salvar($usu)
        {
            DB::beginTransaction();
            $usuario = User::create([
                'name' => $usu['name'],
                'email' => $usu['email'],
                'password' => bcrypt($usu['password'])
            ]);
    
            //verifica se tera algum erro
            if ($usuario) {
                DB::commit();
    
                return [
                    'success' => true,
                    'message' => 'Usuário(a) cadastrado com sucesso!'
                ];
            } else {
                DB::rollback();
    
                return [
                    'sucess' => false,
                    'message' => 'Falha ao cadastrar Usuário(a)'
                ];
            }
    
            //dd($usuario);
        }
    //relacionamento com Contas a Receber
    public function contasReceberas()
    {
        return $this->hasMany(ContasReceber::class);
    }

    //relacionamento com Contas a Pagar    
    public function contasPagaras()
    {
        return $this->hasMany(ContasPagar::class);
    }

    //relacionamento com Despesas    
    public function despesas()
    {
        return $this->hasMany(Despesa::class);
    }

    //relacionamento com Fluxo de Caixa    
    public function fluxoCaixas()
    {
        return $this->hasOne(FluxoCaixa::class);
    }
}