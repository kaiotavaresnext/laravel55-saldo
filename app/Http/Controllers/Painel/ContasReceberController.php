<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContasReceber;
use App\Http\Requests\ContasReceberFormRequest;

class ContasReceberController extends Controller
{
    private $totalPage = 5;

    public function index()
    {
        $contasReceber = new ContasReceber;
        //dd($contasReceberas = $contasReceber->listar());
        $contasReceberas = $contasReceber->listar();

        $situacoes = $contasReceber->situacao();     

        return view('painel.contasReceberas.index',compact('contasReceberas', 'situacoes'));
    }

    public function novo()
    {
        return view('painel.contasReceberas.novo');
    }

    
    public function store(ContasReceberFormRequest $request, ContasReceber $contasReceber)
    {
        //dd($result = $contasReceber->salvar($request->all()));
        $result = $contasReceber->salvar($request->all());

        //mostra mensagem para o usuário na view index
        if ($result['success']) {
            return redirect()
                    ->route('contasReceberas.index')
                    ->with('success', $result['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $result['message']);
 
        }
    }

    public function mostrar($id)
    {
        //dd($contasReceber = ContasReceber::findOrFail($id));
        $contasReceber = ContasReceber::findOrFail($id);
        return view('painel.contasReceberas.mostrar', compact('contasReceber'));
    }

    public function editar($id)
    {
        $contasReceber = ContasReceber::findOrFail($id);
        return view('painel.contasReceberas.editar', compact('contasReceber'));
    }

    public function atualizar(ContasReceberFormRequest $request, $id)
    {
        $contasReceber = ContasReceber::findOrFail($id);
        $atualiza = $contasReceber->update($request->all());  
        
        if ($atualiza) {
            return redirect()
                    ->route('contasReceberas.index')
                    ->with('success', 'Conta atualizada com sucesso!');
            return redirect()
                    ->back()
                    ->with('error', 'Erro ao atualizar conta!');
        } 

    }

    public function deletar(Request $id, ContasReceber $contasReceber)
    {
        //dd($id->toArray());
        $delete = $contasReceber->deleteID($id->delete);
        
        //mostra mensagem para o usuário na view index
        if ($delete['success']) {
            return redirect()
                    ->route('contasReceberas.index')
                    ->with('success', $delete['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $delete['message']);
 
        }
    }
        
    public function pesquisa(Request $request, ContasReceber $contasReceber)
    {
        
        $dataForm = $request->except('_token');

        $contasReceberas = $contasReceber->pesquisa($dataForm, $this->totalPage);

        $situacoes = $contasReceber->situacao();


        return view('painel.contasReceberas.index',compact('contasReceberas', 'situacoes','dataForm'));

    }
    
}

