<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProfileFormRequest;
use App\Http\Requests\UserFormRequest;
use App\User;

class UserController extends Controller
{

    //basta mudar o valor de acordo com a necessidade
    private $qntdUsers = 2; 

    public function index()
    {
        $user = new User;
        $users = $user->listar();
        $countUsers = (count($users) >= $this->qntdUsers);
        //dd($countUsers);

        return view('painel.user.index', compact('users', 'countUsers'));
    }

    //metodo para mostrar a view para preencher dados de um novo usuario
    public function novo()
    {   $user = new User;
        $users = $user->listar();

        if (count($users) >= $this->qntdUsers) {
            return redirect()->back();
        } else {
            return view('painel.user.novo');
        }
    }
    //metodo para salvar novo usuario
    public function store(UserFormRequest $request)
    {
        //dd($request->all());
        $user = new User;
        //dd($user->salvar($request->all()));
        $result = $user->salvar($request->all());

        //dd($result);

        //mostra mensagem para o usuário na view index
        if ($result['success']) {
            return redirect()
                    ->route('usuarios.index')
                    ->with('success', $result['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $result['message']);
 
        }
        
    }

}
