<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Despesa;
use App\Http\Requests\DespesaFormRequest;

class DespesaController extends Controller
{   
    private $totalPage = 50;
    
    public function index()
    {
        $despesa = new Despesa;
        //dd($despesas = $despesa->listar());
        $despesas = $despesa->listar();
        return view('painel.despesas.index',compact('despesas'));
    }

    public function novo()
    {
        return view('painel.despesas.novo');
    }

    
    public function store(DespesaFormRequest $request, Despesa $despesa)
    {
        //dd($result = $despesa->salvar($request->all()));
        $result = $despesa->salvar($request->all());

        //mostra mensagem para o usuário na view index
        if ($result['success']) {
            return redirect()
                    ->route('despesas.index')
                    ->with('success', $result['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $result['message']);
 
        }
    }

    public function mostrar($id)
    {
        //dd($despesa = Despesa::findOrFail($id));
        $despesa = Despesa::findOrFail($id);
        return view('painel.despesas.mostrar', compact('despesa'));
    }

    public function editar($id)
    {
        $despesa = Despesa::findOrFail($id);
        return view('painel.despesas.editar', compact('despesa'));
    }

    public function atualizar(DespesaFormRequest $request, $id)
    {
        $despesa = Despesa::findOrFail($id);
        $atualiza = $despesa->update($request->all());  
        
        if ($atualiza) {
            return redirect()
                    ->route('despesas.index')
                    ->with('success', 'Despesa atualizada com sucesso!');
            return redirect()
                    ->back()
                    ->with('error', 'Erro ao atualizar Despesa!');
        } 

    }

    public function deletar(Request $id, Despesa $despesa)
    {
        //dd($id->toArray());
        $delete = $despesa->deleteID($id->delete);
        
        //mostra mensagem para o usuário na view index
        if ($delete['success']) {
            return redirect()
                    ->route('despesas.index')
                    ->with('success', $delete['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $delete['message']);
 
        }
    }
    

}

