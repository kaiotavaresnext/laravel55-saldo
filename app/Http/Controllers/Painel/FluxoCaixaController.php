<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FluxoCaixa;
use App\Models\ContasReceber;
use App\Models\ContasPagar;
use App\User;
use DB;


class FluxoCaixaController extends Controller
{
    //private $totalPage = 50;

    public function index()
    {
        //$contas = $contasPagar = ContasPagar::where('situacao', 'pago')->get();
        $contas_pg = DB::table('contas_pagaras')->select('valor')->get();
        $contas_rec = DB::table('contas_receberas')->select('valor')->get();
       // dd($contasReceber = ContasReceber::find(1)->paginate($this->totalPage));


        $total_pg = $contas_pg->sum('valor');
        $total_rec = $contas_rec->sum('valor');
        $saldo = $total_rec - $total_pg;
        //echo "<b>VALOR CONTAS A PAGAR</b>: $total_pg <br>";
        //echo "<b>VALOR CONTAS A RECEBER</b>: $total_rec <br>";
        
        return view('painel.fluxoCaixas.index', compact ('total_pg','total_rec','saldo'));
     }

}