<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ContasPagar;
use App\Http\Requests\ContasPagarFormRequest;

class ContasPagarController extends Controller
{      
    private $totalPage = 4;
    
    public function index()
    {
        $contasPagar = new ContasPagar;
        //dd($contasPagaras = $contasPagar->listar());
        $contasPagaras = $contasPagar->listar();

        $situacoes = $contasPagar->situacao();

        return view('painel.contasPagaras.index',compact('contasPagaras', 'situacoes'));
    }

    public function novo()
    {
        return view('painel.contasPagaras.novo');
    }

    
    public function store(ContasPagarFormRequest $request, ContasPagar $contasPagar)
    {
        //dd($result = $contasPagar->salvar($request->all()));
        $result = $contasPagar->salvar($request->all());

        //mostra mensagem para o usuário na view index
        if ($result['success']) {
            return redirect()
                    ->route('contasPagaras.index')
                    ->with('success', $result['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $result['message']);
 
        }
    }

    public function mostrar($id)
    {
        //dd($contasPagar = ContasPagar::findOrFail($id));
        $contasPagar = ContasPagar::findOrFail($id);
        return view('painel.contasPagaras.mostrar', compact('contasPagar'));
    }

    public function editar($id)
    {
        $contasPagar = ContasPagar::findOrFail($id);
        return view('painel.contasPagaras.editar', compact('contasPagar'));
    }

    public function atualizar(ContasPagarFormRequest $request, $id)
    {
        $contasPagar = ContasPagar::findOrFail($id);
        $atualiza = $contasPagar->update($request->all());  
        
        if ($atualiza) {
            return redirect()
                    ->route('contasPagaras.index')
                    ->with('success', 'Conta atualizado com sucesso!');
            return redirect()
                    ->back()
                    ->with('error', 'Erro ao atualizar o conta!');
        } 

    }

    public function deletar(Request $id, ContasPagar $contasPagar)
    {
        //dd($id->toArray());
        $delete = $contasPagar->deleteID($id->delete);
        
        //mostra mensagem para o usuário na view index
        if ($delete['success']) {
            return redirect()
                    ->route('contasPagaras.index')
                    ->with('success', $delete['message']);

            // Redireciona de volta com uma mensagem de erro
            return redirect()
                ->back()
                ->with('error', $delete['message']);
 
        }
    }
    public function pesquisa(Request $request, ContasPagar $contasPagar)
    {
        
        $contasPagaras = $contasPagar->pesquisa($request->advogado, $this->totalPage);

        $title = "Advogados, filtros para: {$request->advogado}";

        return view('painel.contasPagaras.index', compact('title', 'contasPagaras'));

        //return view('painel.contasPagaras.index',compact('contasPagaras', 'situacoes', 'dataForm'));

    }
    
}

