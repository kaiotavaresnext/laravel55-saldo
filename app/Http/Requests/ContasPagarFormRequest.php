<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContasPagarFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'advogado' => 'required',
            'situacao'=> 'required',
            'tipo_despesa'=> 'required',
            'valor'=> 'required',
            'vencimento'=> 'required',
            'form_pagamento'=> 'required',
        ];
    }
}
