<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Sistema de Saldo',

    'title_prefix' => '',

    'title_postfix' => ' - Sistema de Saldo',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b> Saldo </b> 1.0 ',

    'logo_mini' => '<b>Saldo</b> 1.0',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'blue',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'painel',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        'MENU PRINCIPAL',
        [
            'text'    => 'Dashboard',
            'icon'    => 'home',
            'url'  => 'painel',
        ],
        
        [
            'text'    => 'Usuários',
            'icon'    => 'users',
            'url'  => 'painel/usuarios',
        ],

        [
            'text'    => 'Cadastros',
            'icon'    => 'address-book',
            'submenu' => [
                [
                    'text' => 'Advogados',
                    'url'  => 'painel/advogados',
                    'icon' => 'balance-scale',
                ],
                [
                    'text' => 'Estagiários',
                    'url'  => 'painel/estagiarios',
                    'icon' => 'graduation-cap',
                ],
                [
                    'text' => 'Clientes',
                    'url'  => 'painel/clientes',
                    'icon' => 'male',
                ],
                [
                    'text' => 'Colaboradores',
                    'url'  => 'painel/colaboradores',
                    'icon' => ' fa fa-user',
                ],
                [
                    'text' => 'Partes Contrárias',
                    'url'  => 'painel/partes',
                    'icon' => 'fa fa-id-badge',
                ],
                [
                    'text' => 'Processos',
                    'url'  => 'painel/processos',
                    'icon' => 'book',
                ],
            ],
        ],

        [
            'text'    => 'Relatórios',
            'icon'    => 'fa-fw fa fa-files-o',
            'submenu' => [
                [
                    'text' => 'Processos',
                    'url'  => '#',
                    'icon' => 'book',
                ],
                [
                    'text'    => 'Clientes',
                    'url'     => '#',
                    'icon' => 'male',
                ],
            ],
        ],

        [
            'text'    => 'Agendamentos',
            'icon'    => 'calendar',
            'url'  => 'painel/agendamentos',
        ],

        [
            'text'    => 'Utilitários',
            'icon'    => 'cube',
            'submenu' => [
                [
                    'text' => 'Ações',
                    'url'  => 'painel/acoes',
                    'icon' => 'tags',
                ],
                [
                    'text' => 'Instâncias',
                    'url'  => 'painel/instancias',
                    'icon' => 'indent',
                ],
                [
                    'text' => 'Tribunais',
                    'url'  => 'painel/tribunais',
                    'icon' => 'gavel',
                ],
                [
                    'text' => 'Comarcas',
                    'url'  => 'painel/comarcas',
                    'icon' => 'houzz',
                ],
                [
                    'text' => 'Varas/Foros',
                    'url'  => 'painel/varaForos',
                    'icon' => 'hourglass-start',
                ],
                [
                    'text' => 'Modelos de Petições',
                    'url'  => '#',
                    'icon' => 'file',
                ],
            ],
        ],


        [
            'text'    => 'Financeiro',
            'icon'    => 'calculator',
            'submenu' => [
                [
                    'text' => 'Contas a receber',
                    'url'  => 'painel/contasReceberas',
                    'icon' => 'fa-fw fa fa-line-chart',
                ],
                [
                    'text' => 'Contas a pagar',
                    'url'  => 'painel/contasPagaras',
                    'icon' => 'fa-fw fa fa-calculator',
                ],
                [
                    'text' => 'Fluxo de Caixa',
                    'url'  => 'painel/fluxoCaixas',
                    'icon' => 'fa-fw fa fa-calculator',
                ],
            ],
        ],

        [
            'text'    => 'Ajuda',
            'icon'    => 'question-circle',
            'submenu' => [
                [
                    'text' => 'Sobre',
                    'url'  => '#',
                    'icon' => 'info-circle',
                ],
            ],
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];
