<?php

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

//rota para meu perfil
$this->get('painel/perfil/atualizar', 'Painel\UserController@profile')->name('profile')->middleware('auth');
$this->post('perfil', 'Painel\UserController@profileUpdate')->name('profile.update')->middleware('auth');

$this->group(['middleware' => ['auth'], 'namespace' => 'Painel', 'prefix' => 'painel'], function (){

	$this->get('/', 'PainelController@index')->name('painel');
	
	//rotas para filtros
	$this->any('contasPagaras/pesquisa', 'ContasPagarController@pesquisa')->name('contasPagaras.pesquisa');

	//rotas para usuarios	
	$this->get('usuarios', 'UserController@index')->name('usuarios.index');
	$this->get('usuarios/novo', 'UserController@novo')->name('usuarios.create');
	$this->post('usuarios', 'UserController@store')->name('usuarios.store');
	$this->get('usuarios/{id}/editar', 'UserController@editar')->name('usuarios.edit');
	
	//rotas para Contas a Pagar	
	$this->get('contasPagaras', 'ContasPagarController@index')->name('contasPagaras.index');
	$this->get('contasPagaras/novo', 'ContasPagarController@novo')->name('contasPagaras.create');
	$this->get('contasPagaras/{id}', 'ContasPagarController@mostrar')->name('contasPagaras.show');
	$this->get('contasPagaras/{id}/editar', 'ContasPagarController@editar')->name('contasPagaras.edit');
    $this->put('contasPagaras/{id}', 'ContasPagarController@atualizar')->name('contasPagaras.update');
	$this->post('contasPagaras', 'ContasPagarController@store')->name('contasPagaras.store');
	$this->post('contasPagaras/destroy', 'ContasPagarController@deletar')->name('contasPagaras.delete');
	
    
    //rotas para Contas a Receber 	
	$this->get('contasReceberas', 'ContasReceberController@index')->name('contasReceberas.index');
	$this->get('contasReceberas/novo', 'ContasReceberController@novo')->name('contasReceberas.create');
	$this->get('contasReceberas/{id}', 'ContasReceberController@mostrar')->name('contasReceberas.show');
	$this->get('contasReceberas/{id}/editar', 'ContasReceberController@editar')->name('contasReceberas.edit');
    $this->put('contasReceberas/{id}', 'ContasReceberController@atualizar')->name('contasReceberas.update');
	$this->post('contasReceberas', 'ContasReceberController@store')->name('contasReceberas.store');
	$this->post('contasReceberas/destroy', 'ContasReceberController@deletar')->name('contasReceberas.delete');
	$this->post('contasReceberas/pesquisa', 'ContasReceberController@pesquisa')->name('contasReceberas.pesquisa');
	

	//rotas para despesas 	
	$this->get('despesas', 'DespesaController@index')->name('despesas.index');
	$this->get('despesas/novo', 'DespesaController@novo')->name('despesas.create');
	$this->get('despesas/{id}', 'DespesaController@mostrar')->name('despesas.show');
	$this->get('despesas/{id}/editar', 'DespesaController@editar')->name('despesas.edit');
    $this->put('despesas/{id}', 'DespesaController@atualizar')->name('despesas.update');
	$this->post('despesas', 'DespesaController@store')->name('despesas.store');
    $this->post('despesas/destroy', 'DespesaController@deletar')->name('despesas.delete');

    //rotas para Fluxo de Caixa 	
	$this->get('fluxoCaixas', 'FluxoCaixaController@index')->name('fluxoCaixas.index');
	$this->get('fluxoCaixas/{id}', 'FluxoCaixaController@mostrar')->name('fluxoCaixas.show');

	//rotas para usuarios	
	$this->get('usuarios', 'UserController@index')->name('usuarios.index');
	$this->get('usuarios/novo', 'UserController@novo')->name('usuarios.create');
	$this->post('usuarios', 'UserController@store')->name('usuarios.store');

});