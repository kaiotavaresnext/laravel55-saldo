<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContasRecebersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contas_receberas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
                    
            $table->enum('status', ['ativo','inativo']);
            $table->enum('situacao', ['recebido','pendente','a vencer','vencido']);
            $table->string('nome_cliente')->nullable();
            $table->string('tipo_despesa');
            $table->enum('form_pagamento', ['dinheiro','cartao credito','cheque','boleto','deposito','debito']);            
            $table->double('valor', 10, 2);
            $table->double('juros', 10, 2)->nullable();
            $table->double('multa',10 ,2)->nullable();
            $table->double('desconto',10 ,2)->nullable();
            $table->date('vencimento');
            $table->text('descricao')->nullable();
            
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contas_receberas');
    }
}
