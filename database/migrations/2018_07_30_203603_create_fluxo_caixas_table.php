<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFluxoCaixasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fluxo_caixas', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');

            $table->integer('contasReceber_id')->unsigned();
            $table->foreign('contasReceber_id')      
                    ->references('id')
                    ->on('contasReceberas')
                    ->onDelete('cascade');

            $table->integer('despesa_id')->unsigned();
            $table->foreign('despesa_id')      
                    ->references('id')
                    ->on('despesas')
                    ->onDelete('cascade');

            $table->integer('contasPagar_id')->unsigned();
            $table->foreign('contasPagar_id')
                    ->references('id')
                    ->on('contasPagaras')
                    ->onDelete('cascade');

            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fluxo_caixas');
    }
}
