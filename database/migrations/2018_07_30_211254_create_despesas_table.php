<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDespesasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('despesas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            
            $table->enum('status', ['ativo','inativo']);
            $table->string('tipo_despesa');
            $table->double('valor');
            $table->enum('form_pagamento', ['dinheiro','cartao credito','cheque','boleto','deposito','debito']);                        
            $table->date('data_despesa');
            $table->string('descricao')->nullable();
            $table->timestamps();
            

             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('despesas');
    }
}
