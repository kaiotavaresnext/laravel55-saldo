@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop
@section('content')

@include('painel.includes.alerts')



<div class="box box-solid box-primary">
    <div class="box-header">
        <i id="icon" class="fa fa-balance-scale"></i>
        <h3 class="box-title">Editar Conta</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <ol class="breadcrumb">
        <li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('contasReceberas.index') }}"><i class="fa fa-tag"></i> Contas a Receber</a></li>     
        <li><i class="fa fa-edit"></i> Editar</li>
    </ol>

    <div class="box-body">

        <!-- formulario -->
        <form role="form" action="{{ route('contasReceberas.update', [$contasReceber->id]) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
            
            <!-- Cliente -->
            <div class="form-group col-sm-4">
                <label class="col-sm-0 col-form-label col-form-label-sm"style="color: red;">Cliente</label>
                <input type="text" class="form-control input-sm" 
                name="nome_cliente"  value="{{  $contasReceber->nome_cliente }}">
            </div>
            <!-- Final Cliente-->

             <!-- Situação -->
            <div class="form-group col-md-2">
                <label for="situacao"><span style="color:#ea1e1e">Situação</span></label>
                <select class="custom-select form-control input-sm" 
                name="situacao">
                 @if($contasReceber->situacao == 'recebido')
                    <option value="recebido" selcted>Recebido</option>
                    <option value="pendente">Pendente</option>
                    <option value="a vencer">A vencer</option>
                    <option value="vencido">Vencido</option> 
                @elseif($contasReceber->situacao == 'pendente')
                    <option value="pendente" selcted>Pendente</option>
                    <option value="recebido">Recebido</option>
                    <option value="a vencer">A vencer</option>
                    <option value="vencido">Vencido</option> 
                @elseif($contasReceber->situacao == 'a vencer')
                    <option value="a vencer" selcted>A vencer</option>
                    <option value="pendente">Pendente</option>
                    <option value="recebido">Recebido</option>
                    <option value="vencido">Vencido</option> 
                @else
                    <option value="vencido" selcted>Vencido</option>
                    <option value="a vencer">A vencer</option>
                    <option value="pendente">Pendente</option>
                    <option value="recebido">Recebido</option> 
                @endif                                
                </select>
            </div>
            <!-- Final Situação -->

            <!-- Situação -->
            <div class="form-group col-md-3">
                <label for="form_pagamento"><span style="color:#ea1e1e">Formas de Pagamento</span></label>
                <select class="custom-select form-control input-sm" 
                name="form_pagamento">
                @if($contasReceber->form_pagamento == 'dinheiro')
                    <option value="dinheiro" selcted>Dinheiro</option>
                    <option value="cartao credito">Cartão de Crédito</option>
                    <option value="cheque">Cheque</option>
                    <option value="boleto">Boleto</option>
                    <option value="deposito">Depósito</option>
                    <option value="debito">Débito</option>  		
                @elseif($contasReceber->form_pagamento == 'cartao credito')
                    <option value="cartao credito" selcted>Cartão de Crédito</option>
                    <option value="dinheiro">Dinheiro</option>
                    <option value="cheque">Cheque</option>
                    <option value="boleto">Boleto</option>
                    <option value="deposito">Depósito</option>
                    <option value="debito">Débito</option> 
                @elseif($contasReceber->form_pagamento == 'cheque')
                    <option value="cheque" selcted>Cheque</option>
                    <option value="cartao credito">Cartão de Crédito</option>
                    <option value="dinheiro">Dinheiro</option>
                    <option value="boleto">Boleto</option>
                    <option value="deposito">Depósito</option>
                    <option value="debito">Débito</option>
                @elseif($contasReceber->form_pagamento == 'Boleto')
                    <option value="boleto" selcted>Boleto</option>
                    <option value="cheque">Cheque</option>
                    <option value="cartao credito">Cartão de Crédito</option>
                    <option value="dinheiro">Dinheiro</option>
                    <option value="deposito">Depósito</option>
                    <option value="debito">Débito</option>  
                @elseif($contasReceber->form_pagamento == 'Depósito')
                    <option value="deposito" selcted>Depósito</option>
                    <option value="boleto">Boleto</option>
                    <option value="cheque">Cheque</option>
                    <option value="cartao credito">Cartão de Crédito</option>
                    <option value="dinheiro">Dinheiro</option>
                    <option value="debito">Débito</option>  
                @else
                    <option value="debito" selcted>Débito</option>
                    <option value="deposito">Depósito</option>
                    <option value="boleto">Boleto</option>
                    <option value="cheque">Cheque</option>
                    <option value="cartao credito">Cartão de Crédito</option>
                    <option value="dinheiro">Dinheiro</option> 
                @endif                                
                </select>
            </div>
            <!-- Final Situação -->

            <!-- Tipo de Despesa -->
            <div class="form-group col-sm-3">
                <label class="col-sm-0 col-form-label col-form-label-sm"style="color: red;">Tipo de Despesa</label>
                <input type="text" class="form-control input-sm" name="tipo_despesa" value="{{  $contasReceber->tipo_despesa }}">
            </div>
            <!-- Final Tipo de Despesa-->

            <!-- Valor -->
            <div class="form-group col-sm-3">
                <label class="col-sm-0 col-form-label col-form-label-sm"style="color: red;">Valor</label>
                <input type="double" class="form-control input-sm" name="valor" value="{{  $contasReceber->valor }}">
            </div>
            <!-- Final Valor-->

            <!-- Juros -->
            <div class="form-group col-sm-2">
                <label class="col-sm-0 col-form-label col-form-label-sm">Juros</label>
                <input type="double" class="form-control input-sm" name="juros"  value="{{  $contasReceber->juros }}">
            </div>
            <!-- Final Juros-->

             <!-- Multa -->
             <div class="form-group col-sm-2">
                <label class="col-sm-0 col-form-label col-form-label-sm">Multa</label>
                <input type="double" class="form-control input-sm" name="multa"  value="{{  $contasReceber->multa }}">
            </div>
            <!-- Final Juros--> 
            
             <!-- desconto -->
             <div class="form-group col-sm-2">
                <label class="col-sm-0 col-form-label col-form-label-sm">Desconto</label>
                <input type="double" class="form-control input-sm" name="desconto"  value="{{  $contasReceber->desconto }}">
            </div>
            <!-- Final desconto-->

            <!-- Vencimento -->
            <div class="form-group col-sm-3">
                <label class="col-sm-0 col-form-label col-form-label-sm"style="color: red;">Vencimento</label>
                <input type="date" class="form-control input-sm" name="vencimento"  value="{{  $contasReceber->vencimento }}">
            </div>
            <!-- Final Vencimento-->

            <!-- descrição -->
            <div class="form-group col-md-12">
                <label for="descricao">Descrições adicionais</label>
                <textarea type="text" class="form-control input-sm" name="descricao">{{  $contasReceber->descricao }}</textarea>
            </div>
            <!-- fim descrição -->

            <div class="row" style="margin-left: 0">
                <!-- botoes -->                    
                <div class="form-group" style="margin-left: 15px">	
                    <button type="submit" class="btn btn-success btn-sm"><b class="fa fa-paper-plane"></b> Salvar Novo</button>
                    <a href="{{ route('contasReceberas.index') }}" class="btn btn-primary btn-sm"><b class="fa fa-reply-all"></b> Voltar à Lista</a>
                </div>
                <!-- fim botoes -->
            </div>
        </form>
        <!-- fim formulario -->
    </div>
</div>     
@stop
        