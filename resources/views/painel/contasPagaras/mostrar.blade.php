@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop

@section('content')

@include('painel.includes.alerts')

<!-- box-solid -->
<div class="box box-solid box-primary">
	<!-- box-header -->
	<div class="box-header">
        <i id="icon" class="fa fa-tags"></i>
        <h3 class="box-title">Informações</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
	<!-- fim box-header -->

    <ol class="breadcrumb">
        <li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('contasPagaras.index') }}"><i class="fa fa-tag"></i> ações</a></li>     
        <li><i class="fa fa-list-alt"></i> Mostrar</li>
    </ol>

    <!-- box-body -->
    <div class="box-body">
		<!-- box-header -->
		<div class="box-header pull-right">
            <a href="{{ route('contasPagaras.create') }}" class="btn btn-warning btn-sm">
			<i class="ion-person-add"></i> Adicionar novo</a>

			<a href="{{ route('contasPagaras.index') }}" class="btn btn-success btn-sm">
			<i class="fa fa-clipboard"></i> Voltar à lista</a>
		</div>
		<!-- fim box-header -->

        <div class="text-justify">
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <td><strong>Código</strong></td>
                        <td>{{ $contasPagar->id }}</td>
                    </tr>

                    <tr>
                        <td><strong>Advogado</strong></td>
                        <td>{{ $contasPagar->advogado }}</td>
                    </tr>

                    <tr>
                        <td><strong>Situação</strong></td>
                        <td>{{ $contasPagar->situacao }}</td>
                    </tr>

                    <tr>
                        <td><strong>Forma de Pagamento</strong></td>
                        <td>{{ $contasPagar->form_pagamento }}</td>
                    </tr>

                    <tr>
                        <td><strong>Tipo de Despesa</strong></td>
                        <td>{{ $contasPagar->tipo_despesa }}</td>
                    </tr>

                    <tr>
                        <td><strong>Valor</strong></td>
                        <td>{{ $contasPagar->valor }}</td>
                    </tr>
                    
                    <tr>
                        <td><strong>Juros</strong></td>
                        <td>{{ $contasPagar->juros }}</td>
                    </tr>

                    <tr>
                        <td><strong>Multa</strong></td>
                        <td>{{ $contasPagar->multa }}</td>
                    </tr>
                    
                    <tr>
                        <td><strong>Desconto</strong></td>
                        <td>{{ $contasPagar->desconto }}</td>
                    </tr>

                    <tr>
                        <td><strong>Vencimento</strong></td>
                        <td>{{ $contasPagar->vencimento }}</td>
                    </tr>

                    <tr>
                        <td><strong>Descrição</strong></td>
                        <td>{{ $contasPagar->descricao }}</td>
                    </tr>

                    <tr>
                        <td><strong>Criado em</strong></td>
                        <td>{{ $contasPagar->created_at -> format('d/m/Y - H:i') }}</td>
                    </tr>

                    <tr>
                        <td><strong>Atualizado em</strong></td>
                        <td>{{ $contasPagar->updated_at -> format('d/m/Y - H:i') }}</td>
                    </tr>
                    
                    
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- fim box-body -->
</div>
<!-- fim box-solid -->
@stop
	