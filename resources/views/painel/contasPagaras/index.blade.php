@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop

@section('content')

@include('painel.includes.alerts')


<!-- box-solid -->
<div class="box box-solid box-primary">
	<!-- box-header -->
	<div class="box-header">
        <i id="icon" class="fa fa-tags"></i>
        <h3 class="box-title">Lista de Contas a Pagar</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
	<!-- fim box-header -->	

	<ol class="breadcrumb">
		<li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li> <i class="fa fa-indent"></i> Contas a Pagar</li>
	</ol>

	<!-- box-body -->
	<div class="box-body">
		<!-- box-header -->
		<div class="box-header">
			<a href="{{ route('contasPagaras.create') }}" class="btn btn-success btn-sm">
			<i class="ion-person-add"></i> Adicionar Novo</a>

			<!-- Filtro de Pesquisa -->
			<form action="{{ route ('contasPagaras.pesquisa')}}" method="POST" class="form form-inline" >
					<br>
					{!! csrf_field() !!}
					<input type="text" name="advogado" class="form-control" placeholder="Nome do Advogado">

					<button type="submit" class="btn btn-primary">Pesquisar</button>
			</form>
		</div>

		<!-- box-body da tabela -->
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table class="table table-striped table-sm">
							<tbody>
								<tr>
									<th><i class="fa fa-list-ul"></i> Advogado Responsavel</th>
									<th> <i class="fa fa-map-marker"></i> Situação</th>
									<th><i class="fa fa-phone"></i> Tipo de Despesa</th>
									<th><i class="fa fa-phone"></i> Valor</th>
									<th><i class="fa fa-phone"></i> Vencimento</th>																											
									<th><i class="fa fa-gear"></i> Ações</th>
								</tr>
								@foreach($contasPagaras as $contasPagar)
								<tr>
									<td>{{ $contasPagar -> advogado }}</a></td>
									<td>{{ $contasPagar -> situacao }}</td>
									<td>{{ $contasPagar -> tipo_despesa }}</td>									
									<td>{{ $contasPagar -> valor }}</td>	
									<td>{{ $contasPagar -> vencimento }}</td>

								<td>
										<!-- botao visualizar -->
											<div class="btn-group pull-left" role="group" aria-label="Basic example">
												<a href="{{ route('contasPagaras.show', [$contasPagar->id]) }}">
												<button type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="left" 
												title="Vizualizar">
													<i class="fa fa-eye"></i>
												</button>
											</a>&nbsp;
										</div>

										<!-- botao Editar -->
										<div class="btn-group pull-left" role="group" aria-label="Basic example">
											<a href="{{ URL::to('painel/contasPagaras/'.$contasPagar->id.'/editar') }}">
											<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left" 
												title="Editar">
													<i class="ion-edit"></i>
												</button>
											</a>&nbsp;
										</div>

										<!-- botao excluir -->
										<a href="#" data-toggle="tooltip" data-placement="right" 
												title="Excluir">
											<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" 
											data-target="#excluir{{$contasPagar->id}}">
											<i class="fa fa-trash"></i>
										</button>

										<!-- Modal -->
										<div class="modal fade" id="excluir{{$contasPagar->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Confirmar Operação</h4>
												</div>
												<div class="modal-body">
													<p class="text-center">
														Excluir a Conta <strong>{{$contasPagar->tipo_despesa}}</strong>?
													</p>
												</div>
													<div class="modal-footer">
														<form action="{{ route('contasPagaras.delete') }}" method="POST">
															<input type="hidden" name="delete" value="{{$contasPagar->id}}">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															
															<button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancelar</button>
															<button type="submit" class="btn btn-danger btn-xs">Confirmar</button>
														</form>
													</div>
												</div>
											</div>
										</div>
										<!-- Modal -->
									</td>
								</tr>
								@endforeach
				 			</tbody>
			  			</table>
						<!-- paginação -->
							{!! $contasPagaras->links() !!}
					</div>
				</div>
  			</div>
		</div>
		<!-- fim box-body da tabela -->
	</div>
	<!-- fim box-body -->		
</div>
<!-- fim box-solid -->

@stop