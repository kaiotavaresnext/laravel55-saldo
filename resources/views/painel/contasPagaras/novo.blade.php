@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop
@section('content')

@include('painel.includes.alerts')


<div class="box box-solid box-primary">
    <div class="box-header">
        <i id="icon" class="fa fa-user-plus"></i>
        <h3 class="box-title">Nova Conta a Pagar</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <ol class="breadcrumb">
        <li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('contasPagaras.index') }}"><i class="fa fa-male"></i> Contas</a></li>        
        <li><i class="fa fa-user-plus"></i> Novo</li>
    </ol>

    <div class="box-body">        
            <!-- formulario -->
            <form role="form" action="{{ route('contasPagaras.store') }}" method="POST">
            {{ csrf_field() }}

                                <!-- advogado responsavel -->
                            <div id="advogado" class="form-group col-sm-4">
                                <label for="advogado" style="color: red;">Advogado Responsável</label>
                                <input type="text" class="form-control input-sm" name="advogado" value="{{  old('advogado') }}">
                            </div>

                            <!-- advogado responsavel -->

                            <!-- Início Situação -->

                            <div class="form-group col-md-3">
                                <label for="situacao"><span style="color:#ea1e1e">Situação</span></label>
                                <select class="custom-select form-control input-sm" name="situacao">
                                    <option value="" selcted>selecione</option>
                                    <option value="pago">Pago</option>
                                    <option value="pendente">Pendente</option>
                                    <option value="a vencer">A vencer</option>
                                    <option value="vencido">Vencido</option>                                
                                </select>
                            </div>
                            <!-- Fim Situação -->

                            <!--Formas de Pagamento -->
                            <div class="form-group col-md-3">
                                <label for="form_pagamento"><span style="color:#ea1e1e">Forma de Pagamento</span></label>
                                <select class="custom-select form-control input-sm" name="form_pagamento">
                                    <option value="" selcted>selecione</option>
                                    <option value="dinheiro">Dinheiro</option>
                                    <option value="cartao credito">Cartão de Crédito</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="boleto">Boleto</option>
                                    <option value="deposito">Depósito</option>
                                    <option value="debito">Débito</option>  			
                                </select>
                            </div>
                            <!--Final Formas de Pagamento -->
                            
                            <!--Tipo de Despesa-->                            
                            <div id="tipo_despesa" class="form-group col-sm-2">
                                <label for="tipo_despesa" style="color: red;">Tipo de Despesa</label>
                                <input type="text" class="form-control input-sm" name="tipo_despesa" value="{{  old('tipo_despesa') }}">
                            </div>
                            <!--Tipo de Despesa--> 

                            <!--Valor--> 
                            <div id="valor" class="form-group col-sm-3">
                                <label class="col-sm-0 col-form-label col-form-label-sm"style="color: red;">Valor</label>
                                <input type="double" class="form-control input-sm" name="valor"  value="{{  old('valor') }}">
                            </div>
                            <!--Final Valor--> 

                            <!--Juros--> 
                            <div id="juros" class="form-group col-sm-2">
                                <label class="col-sm-0 col-form-label col-form-label-sm">Juros</label>
                                <input type="double" class="form-control input-sm" name="juros"  value="{{  old('juros') }}">
                            </div>
                            <!--Final Juros--> 

                            <!--Multa--> 
                            <div id="multa" class="form-group col-sm-2">
                                <label class="col-sm-0 col-form-label col-form-label-sm" >Multa</label>
                                <input type="double" class="form-control input-sm" name="multa"  value="{{  old('multa') }}">
                            </div>
                            <!--Final Multa-->  

                            <!--Desconto--> 
                            <div id="desconto" class="form-group col-sm-2">
                                <label class="col-sm-0 col-form-label col-form-label-sm" >Desconto</label>
                                <input type="double" class="form-control input-sm" name="desconto"  value="{{  old('desconto') }}">
                            </div>
                            <!--Final Desconto-->  

                            <!--Vencimento--> 
                            <div id="vencimento" class="form-group col-md-3">
                                <label class="col-sm-0 col-form-label col-form-label-sm" style="color: red;">Vencimento</label>
                                <input type="date" class="form-control input-sm" name="vencimento" value="{{  old('vencimento') }}">
                            </div>
                            <!--Final Vencimento--> 
						                                                                    
                            <!-- descrição -->
                            <div class="form-group col-md-12">
                                <label for="descricao">Descrições adicionais</label>
                                <textarea type="text" class="form-control input-sm" id="clienteDescricao" 
                                name="descricao">{{  old('descricao') }}</textarea>
                            </div>
                            <!-- fim descrição -->

                            </div>
                            <!-- fim conteudo de dados da Conta -->   

                            <div class="row" style="margin-left: 0">
                            <!-- botoes -->                    
                            <div class="form-group" style="margin-left: 15px">	
                                <button type="submit" class="btn btn-success btn-sm"><b class="fa fa-paper-plane"></b> Salvar Novo</button>
                                <a href="{{ route('contasPagaras.index') }}" class="btn btn-primary btn-sm"><b class="fa fa-reply-all"></b> Voltar à Lista</a>
                            </div>
                            <!-- fim botoes -->
                    </div>
                </form>
            <!-- fim formulario -->    
        </div>
</div>         
@stop
        