@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop

@section('content')

@include('painel.includes.alerts')


<!-- box-solid -->
<div class="box box-solid box-primary">
	<!-- box-header -->
	<div class="box-header">
        <i id="icon" class="fa fa-users"></i>
        <h3 class="box-title">Lista de Usuários</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
	<!-- fim box-header -->	

	<ol class="breadcrumb">
		<li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><i class="fa fa-users"></i> Usuários</li>
	</ol>

	<!-- box-body -->
	<div class="box-body">
		<!-- box-header -->
		<div class="box-header">
			@if($countUsers)
				<a href="#" class="btn btn-default btn-sm" data-toggle="modal" data-target="#limite">									
				<i class="ion-person-add"></i> Adicionar Novo</a>

				<!-- Modal -->
				<div class="modal fade" id="limite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Alerta</h4>
							</div>
							<div class="modal-body">
								<p class="text-center">
									Limite de Usuários Excedido!
								</p>
							</div>
							<div class="modal-footer">
								<form action="#" method="POST">
									<input type="hidden" name="delete" value="#">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- Modal -->
			@else
				<a href="{{ route('usuarios.create') }}" class="btn btn-success btn-sm">
				<i class="ion-person-add"></i> Adicionar Novo</a>	
			@endif
			
			<div class="input-group input-group-sm pull-right" style="width: 150px;">
				<input type="text" name="table_search" class="form-control pull-right" 
				placeholder="Pesquisar">

				<div class="input-group-btn">
					<button type="submit" class="btn btn-default">
						<i class="fa fa-search"></i>
					</button>
				</div>
			</div>
		</div>
		<!-- fim box-header -->

		<!-- box-body da tabela -->
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table class="table table-striped table-sm table-bordered table-hover">
							<tbody>
								<tr>
									<th><i class="fa fa-user"></i> Nome</th>
									<th><i class="fa fa-envelope"></i> E-mail</th>
									<th><i class="fa fa-plus-square"></i> Criado em</th>
									<th><i class="fa fa-retweet"></i> Atualizado em</th>
									<th><i class="fa fa-cubes"></i> Ações</th>
								</tr>
                                @foreach($users as $user)
								<tr>
									<td>{{ $user->name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->created_at->format('d-m-Y H:i:s') }}</td>
									<td>{{ $user->updated_at->format('d-m-Y H:i:s') }}</td>
									<td>
										<!-- botao editar -->
										<div class="btn-group pull-left" role="group" aria-label="Basic example">
											<a href="{{ route('usuarios.edit', $user->id) }}">
												<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left" 
												title="editar">
													<i class="ion-edit"></i>
												</button>
											</a>&nbsp;
										</div>

										<!-- botao excluir -->
										@if((auth()->user()->email) == '#')
											<a href="#" data-toggle="tooltip" data-placement="right" 
													title="Excluir">
												<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" 
											data-target="#excluir">
												<i class="fa fa-trash"></i>
												</button>
											</a>
										@else
											<a href="#" data-toggle="tooltip" data-placement="right" 
													title="Excluir">
												<button type="button" disabled class="btn btn-danger btn-xs" data-toggle="modal" 
											data-target="#excluir">
												<i class="fa fa-trash"></i>
												</button>
											</a>
										@endif

										<!-- Modal -->
										<div class="modal fade" id="excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Confirmar Operação</h4>
												</div>
												<div class="modal-body">
													<p class="text-center">
														Excluir o Usuário(a) <strong>#</strong>?
													</p>
												</div>
													<div class="modal-footer">
														<form action="#" method="POST">
															<input type="hidden" name="delete" value="#">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">

															<button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancelar</button>
															<button type="submit" class="btn btn-danger btn-xs">Confirmar</button>
														</form>
													</div>
												</div>
											</div>
										</div>
										<!-- Modal -->
									</td>
								</tr>
                                @endforeach
				 			</tbody>
			  			</table>
			  			<!-- paginação -->
			  			{!! $users->links() !!}
					</div>
				</div>
  			</div>
		</div>
		<!-- fim box-body da tabela -->
	</div>
	<!-- fim box-body -->		
</div>
<!-- fim box-solid -->

@stop