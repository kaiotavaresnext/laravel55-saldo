@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop
@section('content')

@include('painel.includes.alerts')

<div class="box box-solid box-primary">
    <div class="box-header">
        <i id="icon" class="fa fa-balance-scale"></i>
        <h3 class="box-title">Cadstrar Usuário</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="#"><i class="fa fa-users"></i> Usuários</a></li>
        <li><i class="fa fa-user-plus"></i> Novo</li>
    </ol>

    <div class="box-body">
        <!-- formulario -->
        <form action="{{ route('usuarios.store') }}" method="POST">
        {{ csrf_field() }}
            <!-- nome completo -->
            <div class="form-group col-md-6">
                <label for="name"><span style="color:#ea1e1e">Nome</span></label>
                <input type="text" class="form-control input-sm" 
                name="name" placeholder="max 20 caracteres" value="{{ old('name')}}">
            </div>  
            <!-- fim nome completo -->

            <!-- E-mail -->
            <div class="form-group col-md-6">
                <label for="email"><span style="color:#ea1e1e">E-mail</span></label>
                <input type="email" class="form-control input-sm"  
                name="email" placeholder="digite um e-mail válido" value="{{ old('email')}}">
            </div>
            <!-- fim E-mail --> 

            <!-- password -->
            <div class="form-group col-md-6">
                <label for="password"><span style="color:#ea1e1e">Senha</span></label>
                <input type="password" class="form-control input-sm" 
                name="password" placeholder="min 6 caracteres" value="{{ old('password')}}">
            </div>
            <!-- fim password -->

            <div class="row form-group col-md-12">
                <!-- botoes -->                    
                <div class="form-group" style="margin-left: 15px">	
                    <button type="submit" class="btn btn-success btn-sm"><b class="fa fa-paper-plane"></b> Salvar Novo</button>
                    <a href="#" class="btn btn-primary btn-sm"><b class="fa fa-reply-all"></b> Voltar à Lista</a>
                </div>
                <!-- fim botoes -->
            </div>
        </form>
        <!-- fim formulario -->       
    </div>
</div>        
@stop
        