@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop
@section('content')

@include('painel.includes.alerts')

<div class="box box-solid box-primary">
    <div class="box-header">
        <i id="icon" class="fa fa-user-plus"></i>
        <h3 class="box-title">Nova Despesa</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <ol class="breadcrumb">
        <li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('despesas.index') }}"><i class="fa fa-male"></i> Contas a Receber</a></li>        
        <li><i class="fa fa-user-plus"></i> Novo</li>
    </ol>

    <div class="box-body">
            <!-- formulario -->
            <form action="{{ route('despesas.store') }}" method="POST">
            {{ csrf_field() }}
                    
                            <!--Tipo de Despesa-->                            
                            <div id="tipo_despesa" class="form-group col-sm-5">
                                <label for="tipo_despesa" style="color: red;">Tipo de Despesa</label>
                                <input type="text" class="form-control input-sm" name="tipo_despesa" value="{{  old('tipo_despesa') }}">
                            </div>
                            <!--Tipo de Despesa-->

                            <!--Formas de Pagamento -->
                            <div class="form-group col-md-3">
                                <label for="form_pagamento"><span style="color:#ea1e1e">Forma de Pagamento</span></label>
                                <select class="custom-select form-control input-sm" name="form_pagamento">
                                    <option value="" selcted>selecione</option>
                                    <option value="dinheiro">Dinheiro</option>
                                    <option value="cartao credito">Cartão de Crédito</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="boleto">Boleto</option>
                                    <option value="deposito">Depósito</option>
                                    <option value="debito">Débito</option>  			
                                </select>
                            </div>
                            <!--Final Formas de Pagamento --> 

                            <!--Valor--> 
                            <div id="valor" class="form-group col-sm-2">
                                <label class="col-sm-0 col-form-label col-form-label-sm"style="color: red;">Valor</label>
                                <input type="double" class="form-control input-sm" name="valor"  value="{{  old('valor') }}">
                            </div>
                            <!--Final Valor--> 

                            <!--Vencimento--> 
                            <div id="data_despesa" class="form-group col-md-2">
                                <label class="col-sm-0 col-form-label col-form-label-sm" style="color: red;">Data de Despesa</label>
                                <input type="date" class="form-control input-sm" name="data_despesa" value="{{  old('data_despesa') }}">
                            </div>
                            <!--Final Vencimento--> 
						                                                                    
                            <!-- descrição -->
                            <div class="form-group col-md-12">
                                <label for="descricao">Descrições adicionais</label>
                                <textarea type="text" class="form-control input-sm" id="clienteDescricao" 
                                name="descricao">{{  old('descricao') }}</textarea>
                            </div>
                            <!-- fim descrição -->

                    </div>
                    <!-- fim conteudo de descrição -->

                    <div class="row">
                        <!-- botoes -->                    
                        <div class="form-group col-md-12" style="margin-left: 15px">	
                            <button type="submit" class="btn btn-success btn-sm"><b class="fa fa-paper-plane"></b> Salvar Nova</button>
                            <a href="{{ route('despesas.index') }}" class="btn btn-primary btn-sm"><b class="fa fa-reply-all"></b> Voltar à Lista</a>
                        </div>
                        <!-- fim botoes -->
                    </div>
            </form>
            <!-- fim formulario -->
             
    </div>
</div>         
@stop
        