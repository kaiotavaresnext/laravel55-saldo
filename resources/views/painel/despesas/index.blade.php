@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop

@section('content')

@include('painel.includes.alerts')


<!-- box-solid -->
<div class="box box-solid box-primary">
	<!-- box-header -->
	<div class="box-header">
        <i id="icon" class="fa fa-tags"></i>
        <h3 class="box-title">Lista de Despesas</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
	<!-- fim box-header -->	

	<ol class="breadcrumb">
		<li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li> <i class="fa fa-indent"></i> Despesas</li>
	</ol>

	<!-- box-body -->
	<div class="box-body">
		<!-- box-header -->
		<div class="box-header">
			<a href="{{ route('despesas.create') }}" class="btn btn-success btn-sm">
			<i class="ion-person-add"></i> Adcionar Nova</a>

			<!-- Filtro de Pesquisa -->
			<form action="{{ route('despesas.search')}}" method="POST" class="form form-inline" >

				<div class="input-group input-group-sm pull-right" style="width: 150px;">
					<input type="text" name="id" class="form-control pull-right" placeholder="Pesquisar">
					<input type="text" name="id" class="form-control pull-right" placeholder="Pesquisar">
				
			
				<div class="input-group-btn">
					<button type="submit" class="btn btn-default">
					<i class="fa fa-search"></i>
					</button>

				</div>
			</div>
		</div>
		</form>
		<!-- fim box-header -->
		<!-- Fim Filtro de Pesquisa -->		

		<!-- box-body da tabela -->
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table class="table table-striped table-sm">
							<tbody>
								<tr>
									<th><i class="fa fa-list-ul"></i> Tipo de Despesa</th>
									<th><i class="fa fa-phone"></i> Valor</th>
									<th><i class="fa fa-phone"></i> Data de Despesa</th>
									<th><i class="fa fa-phone"></i> Forma de Pagamento</th>																																				
									<th><i class="fa fa-gear"></i> Ações</th>
								</tr>
								@foreach($despesas as $despesa)
								<tr>
									<td>{{ $despesa -> tipo_despesa }}</a></td>									
									<td>{{ $despesa -> valor }}</td>	
									<td>{{ $despesa -> data_despesa }}</td>
									<td>{{ $despesa -> form_pagamento }}</td>									

								<td>
										<!-- botao visualizar -->
											<div class="btn-group pull-left" role="group" aria-label="Basic example">
												<a href="{{ route('despesas.show', [$despesa->id]) }}">
												<button type="button" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="left" 
												title="Vizualizar">
													<i class="fa fa-eye"></i>
												</button>
											</a>&nbsp;
										</div>

										<!-- botao Editar -->
										<div class="btn-group pull-left" role="group" aria-label="Basic example">
											<a href="{{ URL::to('painel/despesas/'.$despesa->id.'/editar') }}">
											<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left" 
												title="Editar">
													<i class="ion-edit"></i>
												</button>
											</a>&nbsp;
										</div>

										<!-- botao excluir -->
										<a href="#" data-toggle="tooltip" data-placement="right" 
												title="Excluir">
											<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" 
											data-target="#excluir{{$despesa->id}}">
											<i class="fa fa-trash"></i>
										</button>

										<!-- Modal -->
										<div class="modal fade" id="excluir{{$despesa->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">Confirmar Operação</h4>
												</div>
												<div class="modal-body">
													<p class="text-center">
														Excluir a Conta <strong>{{$despesa->tipo_despesa}}</strong>?
													</p>
												</div>
													<div class="modal-footer">
														<form action="{{ route('despesas.delete') }}" method="POST">
															<input type="hidden" name="delete" value="{{$despesa->id}}">
															<input type="hidden" name="_token" value="{{ csrf_token() }}">
															
															<button type="button" class="btn btn-default btn-xs" data-dismiss="modal">Cancelar</button>
															<button type="submit" class="btn btn-danger btn-xs">Confirmar</button>
														</form>
													</div>
												</div>
											</div>
										</div>
										<!-- Modal -->
									</td>
								</tr>
								@endforeach
				 			</tbody>
			  			</table>
						<!-- paginação -->
			  			{!! $despesas->links() !!}
					</div>
				</div>
  			</div>
		</div>
		<!-- fim box-body da tabela -->
	</div>
	<!-- fim box-body -->		
</div>
<!-- fim box-solid -->

@stop