@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop

@section('content')

@include('painel.includes.alerts')


<!-- box-solid -->
<div class="box box-solid box-primary">
	<!-- box-header -->
	<div class="box-header">
        <i id="icon" class="fa fa-tags"></i>
        <h3 class="box-title">Fluxo de Caixa</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
	<!-- fim box-header -->	

	<ol class="breadcrumb">
		<li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li> <i class="fa fa-indent"></i>Fluxo de Caixa</li>
	</ol>

	
		<!-- box-body da tabela -->
		<div class="box-body">
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<!-- small box de Saldo a Receber-->
					<div class="small-box bg-blue">
						<div class="inner">

							<h3>R$ {{ $contasPagaras->valor }} </h3>

							<p>Total a Receber</p>
							</div>
							<div class="icon">
							<i class="ion ion-stats-bars"></i>
							</div>
							<a href="#" class="small-box-footer">
							Lista de Contas a Receber <i class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>	
					<!-- small box de Saldo a Pagar -->
					<div class="col-lg-6 col-xs-12">
						<div class="small-box bg-blue">
							<div class="inner">

							<h3>R$  </h3>

							<p>Total a Pagar</p>
							</div>
							<div class="icon">
							<i class="ion ion-stats-bars"></i>
							</div>
							<a href="#" class="small-box-footer">
							Lista de Contas a Pagar <i class="fa fa-arrow-circle-right"></i>
							</a>
						</div>
					</div>
  			</div>
		</div>
		<!-- fim box-body da tabela -->
	</div>
	<!-- fim box-body -->		
</div>
<!-- fim box-solid -->

@stop