@extends('adminlte::page')

@section('title', 'ADVSOFT | WEB')

@section('content_header')

@stop

@section('content')

@include('painel.includes.alerts')


<!-- box-solid -->
<div class="box box-solid box-primary">
	<!-- box-header -->
	<div class="box-header">
        <i id="icon" class="fa fa-tags"></i>
        <h3 class="box-title">Fluxo de Caixa</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
	<!-- fim box-header -->	

	<ol class="breadcrumb">
		<li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li> <i class="fa fa-indent"></i>Fluxo de Caixa</li>
	</ol>

	
	<!-- box-body da tabela -->
	<div class="box-body">
		<div class="row">
			<!-- small box de Saldo a Receber-->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box bg-blue">
					<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total a Receber</span>
						<span class="info-box-number"><h3>R$ {{ $total_rec }} </h3></span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- fim box-body da tabela -->
			</div>

			<!-- small box de Saldo a Receber -->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box bg-blue">
					<span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Total a Pagar</span>
						<span class="info-box-number"><h3>R$ {{ $total_pg }} </h3></span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- fim box-body da tabela -->
			</div>

			<!-- small box de Saldo a Receber -->
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="info-box bg-blue">
					<span class="info-box-icon"><i class="ion ion-stats-bars"></i></span>
					<div class="info-box-content">
						<span class="info-box-text">Saldo</span>
						<span class="info-box-number"><h3>R$ {{ $saldo }} </h3></span>	
					</div>
					<!-- /.info-box-content -->
				</div>
			</div>
			<!-- fim box-body da tabela -->

			<table class="table table-striped table-sm">
				<tbody>
					<tr>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
			</table>

		</div>
		<!-- fim box-body -->		
	</div>
	<!-- fim box-solid -->
@stop