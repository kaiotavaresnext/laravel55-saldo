@extends('adminlte::page')

@section('title', 'Cadastrar Ação')

@section('content_header')

@stop
@section('content')

@include('painel.includes.alerts')

<div class="box box-solid box-primary">
    <div class="box-header">
        <i id="icon" class="fa fa-tags"></i>
        <h3 class="box-title">Cadastrar Ação</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-primary btn-sm" data-widget="refresh" title="Atualizar informações">
                <i class="fa fa-refresh"></i>
            </button>
            <button class="btn btn-primary btn-sm" data-widget="collapse" title="Diminuir/Aumentar janela">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>

    <ol class="breadcrumb">
        <li><a href="{{ route('painel') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ route('acoes.index') }}"><i class="fa fa-tag"></i> ações</a></li>     
        <li><i class="fa fa-plus-circle"></i> Novo</li>
    </ol>

    <div class="box-body">

            <!-- formulario -->
            <form role="form" action="{{ route('acoes.store') }}" method="POST">
            {{ csrf_field() }}

                        <!-- Descrição -->
                        <div class="form-group col-md-12">
                            <label for="descricao"><span style="color:#ea1e1e">Descrição</span></label>
                            <input type="text" class="form-control input-sm" 
                            name="descricao" placeholder="digite a descrição aqui..." value="{{ old('descricao')}}">
                        </div>  
                        <!-- Descrição -->  

                    <div class="row" style="margin-left: 0">
                        <!-- botoes -->                    
                        <div class="form-group" style="margin-left: 15px">	
                            <button type="submit" class="btn btn-success btn-sm"><b class="fa fa-paper-plane"></b> Salvar Novo</button>
                            <a href="{{ route('acoes.index') }}" class="btn btn-primary btn-sm"><b class="fa fa-reply-all"></b> Voltar à Lista</a>
                        </div>
                        <!-- fim botoes -->
                    </div>
            </form>
            <!-- fim formulario -->       
    </div>
</div>       
@stop
        